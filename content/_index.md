+++
title = "Clean Code slides"
outputs = ["Reveal"]

+++

# Clean Code

A Handbook of Agile Software Craftsmanship

{{% note %}}

Benvenuti a questa presentazione su libro Clean Code - A Handbook of Agile Software Craftsmanship.


Questa presentazione vuole passare in rassegna quelle che sono indicate come buone pratiche dal testo in oggetto.
Ma andiamo a vedere la scaletta di questo primo incontro. =>
{{% /note %}}

---

## Contents

- Introduction
- Clean Code
- Meaningful Names
- Functions
- Comments
- Formatting

{{% note %}}
Per il primo appuntamento tratteremo un introduzione al software craftsmanship con una definizione di cosa sia il "clean code".
E andrevo via via scendendo nei dettagli che quest'oggi si concentreranno su come chiamare le cose e come trattare le funzioni
Ma proseguiamo =>
{{% /note %}}
