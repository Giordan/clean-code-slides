+++
weight = 30
+++

{{% section %}}

# Meaningful Names

{{% note %}}
I nomi sono ovunque nel software. Per prima cosa dobbiamo dare dei nomi alle cose per scrivere software.
{{% /note %}}

---

## Use Intention-Revealing Names

Il nome di una variabile, funzione o classe ti dice perche' quell'entita' esiste, cosa fa e come si usa.

Se un nome richiede un commento, allora il nome non rivela l'intento dell'entita'.

{{% note %}}
Il problema non e' nella semplicita' del codice, ma nel contesto implicito dello stesso.
Il problema e' nel grado in cui il contesto e' esplicitato nel codice stesso attraverso l'uso corretto dei nomi.
{{% /note %}}

---

## Avoid Disinformation

Spelling similar concepts similarly is information. Using inconsistent spellings is disinformation.

{{% note %}}
I programmatori devono evitare di lasciare indizi falsi che oscurano il significato del codice.
Non riferirti ad un raggruppamento di account come AccountList a meno che non sia effettivamente una lista.
La parola List rappresenta qualcosa di specifico per i programmatori.
{{% /note %}}

---

## Make Meaningful Distinctions

{{% fragment %}}Programmers create problems for themselves when they write code solely to satisfy a compiler or interpreter.<br />
It is not sufficient to add number series or noise words, even though the compiler is satisfied.<br />
Number-series naming (a1, a2, .. aN) is the opposite of intentional naming.<br />
Noise words are another meaningless distinction.<br />
Distinguish names in such a way that the reader knows what the differences offer.{{% /fragment %}}

{{% note %}}
I programmatori creano problemi per se stessi quando scrivono codice solo per soddisfare un compilatore o un interprete.
<br />
La denominazione della serie numerica (a1, a2, .. aN) è l'opposto della denominazione intenzionale.
<br />
Le "Noise words" sono un'altra distinzione senza senso e sono anche ridondanti. La parola "variable" non dovrebbe apparire nel nome di una variabile.
{{% /note %}}

---

## Use Pronounceable Names

{{% fragment %}}Humans are good at words. A significant part of our brains is dedicated to the concept of words.<br />
So make your names pronounceable.<br />
If you can’t pronounce it, you can’t discuss it without sounding like an idiot.<br />
This matters because programming is a social activity.{{% /fragment %}}

{{% note %}}
Le parole sono, per definizione, pronunciabili.
Sarebbe un peccato non approfittare di quell'enorme porzione del nostro cervello che si è evoluta per occuparsi della lingua parlata.
{{% /note %}}

---

## Use Searchable Names

{{% fragment %}}Longer names trump shorter names, and any searchable name trumps a constant in code.<br />
Author's preference is that single-letter names can ONLY be used as local variables inside short methods.<br />
The length of a name should correspond to the size of its scope.<br />
If a variable or constant might be seen or used in multiple places in a body of code, it is imperative to give it a search-friendly name. {{% /fragment %}}

{{% note %}}
I nomi a lettere singole e le costanti numeriche presentano un problema particolare in quanto non sono facili da individuare in un corpo di testo.
La lunghezza di un nome dovrebbe corrispondere alla dimensione del suo ambito.
Se una variabile o una costante può essere vista o utilizzata in più punti in un corpo di codice, è imperativo assegnargli un nome ricercabile.
{{% /note%}}

---

## Avoid Encoding

{{% fragment %}}Encoding type or scope information into names simply adds an extra burden of deciphering.{{% /fragment %}}

{{% note %}}
Abbiamo abbastanza codifiche da affrontare senza aggiungere altro al nostro carico. Il tipo di codifica o le informazioni sull'oscilloscopio nei nomi aggiungono semplicemente un ulteriore carico di decifrazione.
<br />
Imporre ad ogni nuovo dipendente di imparare ancora un altro "linguaggio" di codifica e' un onere mentale inutile quando si cerca di risolvere un problema. Nomi codificati
sono raramente pronunciabili e sono facili da digitare male.
{{% /note %}}

---

**Hungarian Notation**

https://it.wikipedia.org/wiki/Notazione_ungara

---

**Member Prefixes**

People quickly learn to ignore the prefix (or suffix) to see the meaningful part of the name.

The more we read the code, the less we see the prefixes.

Eventually the prefixes become unseen clutter and a marker of older code.

---

**Interfaces and Implementations**

{{% note %}}
Questi possono essere dei casi speciali di codifica.
Per esempio, consideriamo che io voglia creare un' Abstract Factory per la creazione di oggetti "shape", che quindi e' un'interfaccia e che verra' implementata dalla classe concreta.
Come posso nominarle?
IShapeFactory e ShapeFactory.
L'autore preferisce lasciare le interfaccie prive di ornamenti e suggerisce di ornare l'implementazione.
{{% /note %}}

---

## Avoid Mental Mapping

{{% fragment %}}Readers shouldn’t have to mentally translate your names into other names they already know.<br />
One difference between a smart programmer and a professional programmer is that the professional understands that clarity is king.<br />
Professionals use their powers for good and write code that others can understand.{{% /fragment %}}

---

## Class Names

{{% fragment %}}Classes and objects should have noun or noun phrase names like Customer, WikiPage, Account, and AddressParser.<br />
Avoid words like Manager, Processor, Data, or Info in the name of a class.<br />
A class name should not be a verb.{{% /fragment %}}

---

## Method Names


{{% fragment %}}Methods should have verb or verb phrase names like postPayment, deletePage, or save.<br />
Accessors, mutators, and predicates should be named for their value and prefixed with get, set, and is according to the javabean standard.<br />
When constructors are overloaded, use static factory methods with names that describe the arguments. {{% /fragment %}}

---

## Don't Be Cute

{{% fragment %}}Choose clarity over entertainment value.<br />
Cuteness in code often appears in the form of colloquialisms or slang.<br />
Say what you mean. Mean what you say{{% /fragment %}}

---

## Pick One Word per Concept

{{% fragment %}}Pick one word for one abstract concept and stick with it.<br />
The function names have to stand alone, and they have to be consistent in order for you to pick the correct method without any additional exploration.<br />
A consistent lexicon is a great boon to the programmers who must use your code.{{% /fragment %}}

---

## Don't Pun

{{% fragment %}}Avoid using the same word for two purposes. Using the same term for two different ideas is essentially a pun.<br />
Our goal, as authors, is to make our code as easy as possible to understand. We want our code to be a quick skim, not an intense study. We want to use the popular paperback model whereby the author is responsible for making himself clear and not the academic model where it is the scholar’s job to dig the meaning out of the paper.{{% /fragment %}}

{{% note %}}
Non fare giochi di parole. Cioe' non usare le stesse parole per esprimere due concetti diversi.
{{% /note %}}

---

## Use Solution Domain Names

{{% fragment %}}Remember that the people who read your code will be programmers.<br />
There are lots of very technical things that programmers have to do. Choosing technical names for those things is usually the most appropriate course. {{% /fragment %}}

{{% note %}}
Non è saggio usare ogni nome dal dominio del problema perché non vogliamo che i nostri colleghi debbano correre avanti e indietro per chiedere al cliente cosa significhi ogni nome quando già conoscono il concetto con un nome diverso.
Ci sono molte cose molto tecniche che i programmatori devono fare.
La scelta di nomi tecnici per queste cose è solitamente il modo più appropriato.
{{% /note %}}

---

## Use Problem Domain Name

{{% note %}}
Quando non c'e' un corrispondente nel linguaggio dei programmatori per quello che stai facendo, allora usa il nome del dominio del problema.
Separare concetti del dominio del problema dai concetti del dominio della soluzione fa parte del lavoro di un buon programmatore e progettista.
Il codice che ha a che fare con concetti del dominio del problema deve avere nomi estrarti dal dominio del problema.
{{% /note %}}

---

## Add Meaningful Context

{{% note %}}
Ci sono alcuni nomi che hanno un significato in sé e per sé - molti non lo sono.
Invece, è necessario inserire i nomi nel contesto per il lettore racchiudendoli in classi, funzioni o spazi dei nomi ben definiti. Quando tutto il resto fallisce, il prefisso del nome potrebbe essere necessario come ultima risorsa.

Esempio dell'indirizzo con la variabile "state".
{{% /note %}}

---

## Don't Add Gratuitos Context

{{% fragment %}}Shorter names are generally better than longer ones, so long as they are clear.  Add no more context to a name than is necessary.<br />
The resulting names are more precise, which is the point of all naming.{{% /fragment %}}

---

## Final Words

{{% fragment %}}The hardest thing about choosing good names is that it requires good descriptive skills and a shared cultural background.<br />
This is a teaching issue rather than a technical, business, or management issue.<br />
Follow some of these rules and see whether you don’t improve the readability of your code. If you are maintaining someone else’s code, use refactoring tools to help resolve these problems. It will pay off in the short term and continue to pay in the long run. {{% /fragment %}}

{{% note %}}
Le persone hanno anche paura di rinominare le cose per paura che altri sviluppatori si oppongano.
Non condividiamo questa paura e scropriremo che saremo davvero grati quando i nomi cambieranno (in meglio).
Segui alcune di queste regole e verifica se non migliori la leggibilità del tuo codice.
Se stai mantenendo il codice di qualcun altro, usa gli strumenti di refactoring per risolvere questi problemi.
Pagherà a breve termine e continuerà a pagare a lungo termine.
{{% /note%}}
---

{{% /section %}}
