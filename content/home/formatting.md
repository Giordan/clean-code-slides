+++
weight = 60
+++

{{% section %}}

# Formatting

{{% note %}}
Assicuratevi	che	il	vostro	codice	sia	ben	formattato.	Dovreste	scegliere
un	insieme	di	regole	semplici	in	base	alle	quali	formattare	il	vostro	codice	e
poi	dovreste	applicare	coerentemente	tali	regole.	Se	lavorate	in	un	team,
tutto	il	team	dovrebbe	accordarsi	su	un	unico	insieme	di	regole	di
formattazione,	alle	quali	si	atterranno	tutti.	Sarà	utile	anche	impiegare	uno
strumento	automatico	in	grado	di	applicare	tali	regole	di	formattazione	per
voi.
{{% /note %}}

---

## The Purpose of Formatting

{{% note %}}
Prima	di	tutto,	è	importante	essere	chiari:	la	formattazione	del	codice	è
importante.	È	troppo	importante	per	ignorarla	e	troppo	importante	per
trattarla	religiosamente.	La	formattazione	del	codice	è	comunicazione	e	la
comunicazione	è	la	massima	priorità	dello	sviluppatore	professionale.
La	funzionalità	che	create	oggi	ha	buone	probabilità
di	cambiare	nella	prossima	release,	ma	la	leggibilità	del	vostro	codice	avrà
un	effetto	profondo	su	tutte	le	modifiche	che	verranno	eseguite.	Lo	stile	di
programmazione	e	leggibilità	definiscono	dei	precedenti	che	continueranno
a	influenzare	manutenibilità	ed	estendibilità	anche	molto	tempo	dopo	che	il
codice	è	stato	modificato.	Il	vostro	stile	e	la	vostra	disciplina
sopravvivranno,	anche	al	di	là	del	vostro	codice.
{{% /note %}}

---

## Vertical Formatting

{{% fragment %}}
**The Newspaper Metaphor**
{{% /fragment %}}

{{% note %}}
Quanto	dovrebbe	essere	grande un	file	di	codice	sorgente?
I	piccoli	file,	solitamente,	sono	più	facili	da	comprendere	di	quelli
lunghi.
Pensate	a	un	articolo	ben	scritto.	Lo	leggete	verticalmente.	In	cima	vi
aspettate	un	titolo	che	vi	comunichi	l’argomento	e	vi	consenta	di	decidere se	si	tratta	di	qualcosa	di	interesse.
Il	primo	paragrafo	vi	presentarà	l’intera
storia,	evitando	tutti	i	dettagli	e	fornendovi	solo	i	concetti	principali.
Procedendo	verso	il	basso,	aumenteranno	i	dettagli,	fino	a	raggiungere	tutte
le	date,	i	nomi,	le	citazioni,	le	affermazioni	e	così	via.
Un	file	di	codice	sorgente	dovrebbe	essere	un	po’	come	un	articolo.
{{% /section %}}

---

**Vertical Openness Between Concepts**

{{% note %}}
Quasi	tutto	il	codice	si	legge	da	sinistra	a	destra	e	dall’alto	in	basso.	Ogni
riga	rappresenta	un’espressione	o	una	clausola	e	ogni	gruppo	di	righe
rappresenta	un	ragionamento.	Questi	ragionamenti	dovrebbero	essere
separati	dagli	altri	da	alcune	righe	vuote.
Questa	regola,	estremamente	semplice,	ha	un	effetto	profondo	sul
layout	visuale	del	codice.	Ogni	riga	vuota	è	un	indizio	visuale	che	identifica
un	nuovo	concetto,	distinto.	Mentre	scorrete	il	listato,	il	vostro	sguardo
verrà	attratto	dalla	prima	riga	che	segue	una	riga	vuota.
Questo	effetto	è	ancora	più	pronunciato	se	allontanate	lo	sguardo	e	poi	vi
ritornate.	Nel	primo	esempio	i	gruppi	di	righe	saltano	all’occhio,	mentre	nel
secondo	esempio	tutto	si	“impasta”.	La	differenza	fra	questi	due	listati	sta
solo	nella	spaziatura	verticale.
{{% /note %}}

---

**Vertical Density**

{{% note %}}

Se	un’apertura	separa	i	concetti,	la	densità	verticale	implica	una	forte
associazione.	Pertanto	le	righe	di	codice	strettamente	correlate	dovrebbero
presentarsi	verticalmente	“dense”.

{{% /note %}}

---

**Vertical Distance**

{{% fragment %}}Variable Declarations.{{% /fragment %}}
{{% fragment %}}Instance variables.{{% /fragment %}}
{{% fragment %}}Dependent Functions.{{% /fragment %}}
{{% fragment %}}Conceptual Affinity.{{% /fragment %}}
{{% note %}}
Vi	è	mai	capitato	di	vagare	all’interno	di	una	classe,	saltando	da	una
funzione	alla	successiva,	scorrendo	in	lungo	e	in	largo	il	file	di	codice
sorgente,	nel	tentativo	di	indovinare	le	relazioni	fra	le	funzioni,	solo	per
perdervi	in	una	sorta	di	labirinto?	Vi	è	mai	capitato	di	ricercare
affannosamente	la	catena	di	ereditarietà	per	la	definizione	di	una	variabile	o
di	una	funzione?	È	frustrante,	perché	cercate	disperatamente	di
comprendere	che	 cosa 	fa	il	sistema,	ma	vi	trovate	a	sprecare	tempo	ed energie	nel	tentativo	di	individuare,	e	poi	ricordare,	 come 	ricomporre	i
pezzi.

I	concetti	strettamente	correlati	dovrebbero	trovarsi	vicini,	verticalmente.
Per	quei	concetti	che	sono	così	strettamente	correlati	da	appartenere	allo
stesso	file	di	codice	sorgente,	la	separazione	verticale	dovrebbe	essere
un’indicazione	dell’importanza	dell’uno	per	la	comprensibilità	dell’altro.

Dichiarazioni	di	variabili
Le	variabili	dovrebbero	essere	dichiarate	il	più	possibile	vicine	al	luogo
in	cui	vengono	usate.	Poiché	le	nostre	funzioni	sono	molto	corte,	le	variabili
locali	dovrebbero	trovarsi	in	cima	a	ogni	funzione

Variabili	di	istanza
Le	 variabili	di	istanza ,	d’altra	parte,	dovrebbero	essere	dichiarate	in	cima
alla	classe.	Questo	non	dovrebbe	aumentare	la	distanza	verticale	di	queste
variabili,	perché	in	una	classe	ben	progettata,	esse	vengono	usate	da	molti
dei	metodi	della	classe,	se	non	tutti.
Esistono	molte	discussioni	sulla	corretta	collocazione	delle	variabili	di
istanza.	L’importante	è	che	le	variabili	di	istanza	vengano	dichiarate	in
una	posizione	ben	precisa.	Così	tutti	sapranno	dove	andare	a	cercare	le
dichiarazioni.

Funzioni	dipendenti
Se	una	funzione	ne	richiama	un’altra,	esse	dovrebbero	trovarsi
verticalmente	vicine	e	la	chiamante	dovrebbe	essere	sopra	quella
richiamata,	se	possibile.	Ciò	dona	al	programma	un	flusso	naturale.	Se
questa	convenzione	viene	seguita	coerentemente,	coloro	che	leggeranno	il codice	si	renderanno	conto	che	le	definizioni	di	funzioni	si	trovano	poco
dopo	il	loro	uso.

Affinità	concettuale
Alcuni	frammenti	di	codice	“vogliono	stare	vicini”.	Hanno	una	certa
affinità	concettuale.	Più	forte	è	tale	affinità,	minore	distanza	verticale
dovrebbe	separarli.
questa	affinità	può	basarsi	su	una	dipendenza
diretta,	come	quando	una	funzione	ne	richiama	un’altra	o	una	funzione	usa
una	variabile.	Ma	vi	sono	altre	possibili	cause	di	affinità.	Può	essere	dovuta
al	fatto	che	un	gruppo	di	funzioni	svolge	operazioni	simili.

{{% /note %}}

---

**Vertical Ordering**

{{% note %}}
In	generale	vogliamo	che	le	dipendenze	nelle	chiamate	a	funzione
procedano	verso	il	basso.	In	pratica,	una	funzione	che	viene	richiamata
dovrebbe	trovarsi	sotto	una	funzione	che	esegue	la	chiamata.	Questo	crea
un	flusso	armonioso	lungo	il	modulo	di	codice	sorgente,	dai	livelli	superiori
ai	livelli	inferiori.
ci	aspettiamo	che	i	concetti	più	importanti	vengano
per	primi	e	che	siano	espressi	senza	troppo	“inquinamento”	da	parte	dei
dettagli.	Ci	aspettiamo	che	i	dettagli	di	basso	livello	vengano	per	ultimi.
Questo	ci	consente	di	sfogliare	il	file	di	codice	sorgente,	di	farci	un’idea	del
significato	delle	prime	funzioni,	il	tutto	senza	immergerci	nei	dettagli.
{{% /note %}}

---

## Horizontal Formatting

{{% note %}}
Quanto	dovrebbe	essere	larga	una	riga?
È	evidente	che	i	programmatori	prediligono	le	righe	brevi.
Questo	suggerisce	di	preferire	l’impiego	di	righe	brevi.	Il	vecchio	limite
di	Hollerith	di	80	caratteri	è	ormai	un	po’	arbitrario	e	nulla	più	vieta	che	le
righe	siano	lunghe	100	o	anche	120	caratteri.	Ma	probabilmente	non	è	mai
utile	andare	oltre.
{{% /note %}}

---

**Horizontal Openness and Density**

```java
private void measureLine(String line) {
  lineCount++;
  int lineSize = line.length();
  totalChars += lineSize;
  lineWidthHistogram.addLine(lineSize, lineCount);
  recordWidestLine(lineSize);
}
```
{{% note %}}
Usiamo	la	spaziatura	orizzontale	per	associare	quegli	elementi	che	sono
fortemente	correlati	e	per	disassociare	quegli	elementi	che	sono	più
debolmente	correlati.

Gli	operatori	di	assegnamento sono circondati	con	uno	spazio	per
evidenziarli.	Le	istruzioni	di	assegnamento	hanno	principalmente	due
elementi	distinti:	il	lato	sinistro	e	il	lato	destro.	I	due	spazi	evidenziano	tale
separazione.	Non	e' sono stati messi	spazi	fra	i	nomi	di	funzione	e	le	parentesi
aperte.	Questo	perché	la	funzione	e	i	suoi	argomenti	sono	strettamente
correlati.	Separandoli	li	renderemmo	disgiunti	e	non	è	quello	che	vogliamo.
Sono stati separati	gli	argomenti	nelle	parentesi	della	chiamata	a	funzione	per
evidenziare	la	virgola	e	mostrare	che	gli	argomenti	sono	separati.

{{% /note %}}

---

**Horizontal Alignment**

```java
public class FitNesseExpediter implements ResponseSender
{
  private   Socket          socket;
  private   InputStream     input;
  private   OutputStream    output;
  private   Request         request;
  private   Response        response;
  private   FitNesseContext context;
  protected long            requestParsingTimeLimit;
  private   long            requestProgress;
  private   long            requestParsingDeadline;
  private   boolean         hasError;

  public FitNesseExpediter(Socket s, FitNesseContext context) throws Exception
  {
    this.context            = context;
    socket                  = s;
    input                   = s.getInputStream();
    output                  = s.getOutputStream();
    requestParsingTimeLimit = 10000;
  }
  ...
}
```
{{% note %}}
Quando	programmava	in	Assembly 	usava	l’allineamento	orizzontale	per	evidenziare	determinate
strutture.
Ha continuato	a	tentare	di	allineare	tutti	i	nomi	di	variabili	di	un	insieme	di
dichiarazioni	o	tutti	gli	rvalue	di	un	insieme	di	istruzioni	di	assegnamento.
Il	suo	codice	poteva	avere	il	seguente	aspetto:

Tuttavia	ha	scoperto	che	questo	tipo	di	allineamento	non	è	utile.
L’allineamento	sembra	evidenziare	le	cose	sbagliate	e	allontana	lo	sguardo
dal	vero	intento.	Per	esempio,	nella	lista	di	dichiarazioni	precedente	sareste
tentati	di	scorrere	la	lista	dei	nomi	di	variabili	senza	considerarne	il	tipo.
Analogamente,	nella	lista	di	istruzioni	di	assegnamento	sareste	tentati	di
scorrere	la	lista	di	rvalue	senza	nemmeno	considerare	l’operatore	di
assegnamento.
A	peggiorare	le	cose,	gli	strumenti	automatici	di
riformattazione	solitamente	eliminano	questo	tipo	di	allineamento.
Pertanto,	alla	fine,	non	fa'	più	questo	genere	di	cose.	Al	giorno	d’oggi
preferisce	le	dichiarazioni	e	gli	assegnamenti	non	allineati perché	evidenziano	un	importante	difetto:	se	ha	lunghi
elenchi	che	devono	essere	allineati,	 il	problema	sta	nella	lunghezza
dell’elenco ,	non	nella	mancanza	di	allineamento.
{{% /note %}}

---

**Indentation**

{{% fragment %}}Breaking Indentation.{{% /fragment %}}
{{% note %}}
Un	file	di	codice	sorgente	ha	una	struttura	gerarchica.	Vi	sono
informazioni	relative	all’intero	file,	alle	singole	classi	del	file,	ai	metodi
delle	classi,	ai	blocchi	dei	metodi	e	poi	ricorsivamente	ai	sottoblocchi	dei
blocchi.	Ogni	livello	di	questa	gerarchia	è	anche	un	livello	di	visibilità
(scope)	all’interno	del	quale	possono	essere	dichiarati	dei	nomi	e	nel	quale
vengono	interpretate	le	dichiarazioni	e	le	istruzioni	eseguibili.
Per	rendere	più	visibile	questa	gerarchia	di	livelli,	indentiamo	le	righe	di
codice	sorgente	in	proporzione	alla	loro	posizione	nella	gerarchia.	Le
istruzioni	a	livello	del	file,	come	quelle	della	maggior	parte	delle	classi,	non
hanno	alcun	livello	di	indentazione.	I	metodi	all’interno	di	una	classe	sono
indentati	di	un	livello	a	destra	rispetto	alla	classe.	Le	implementazioni	di
tali	metodi	sono	indentate	di	un	livello	a	destra	rispetto	alla	dichiarazione
del	metodo.	Le	implementazioni	dei	blocchi	sono	indentate	di	un	livello	a
destra	rispetto	al	contenitore	e	così	via.
I	programmatori	contano	moltissimo	su	questo	schema	di	indentazione.
Allineano	visivamente	le	righe	per	chiarirne	la	posizione	nella	gerarchia.
Questo	consente	loro	di	individuare	rapidamente	i	livelli,	come	le
implementazioni	delle	istruzioni	 if 	o	 while 	inadatte.	Scorrono	il	margine
sinistro	alla	ricerca	delle	dichiarazioni	di	nuovi	metodi,	nuove	variabili	e anche	nuove	classi.	Senza	indentazione,	i	programmi	sarebbero
praticamente	illeggibili	a	un	occhio	umano.

Infrangere	l’indentazione
Talvolta	si	sarebbe	tentati	di	infrangere	la	regola	di	indentazione	per	i
costrutti	brevi:	istruzioni	 if ,	cicli	 while 	o	funzioni.	Ogni	volta	che	ha	ceduto
a	questa	tentazione,	quasi	sempre	e'	tornato	sui	suoi	passi	e	ha	rimesso l’indentazione.
{{% /note %}}

---

**Dummy Scopes**

```java
while (dis.read(buf, 0, readBufferSize) != -1)
 ;
```
{{% note %}}
Talvolta	il	corpo	di	un’istruzione	 while 	o	 for 	è	fittizio,	come	potete
vedere	di	seguito.	All'autore non	piace	ricorrere	a	questo	tipo	di	strutture	e	tenta	di
evitarle.	Quando	non	puo'	evitarle,	si	assicura	che	il	corpo,	pur	fittizio,
sia	correttamente	indentato	e	circondato	da	parentesi	graffe.
E'stato	ingananto	tante volte da	un	punto	e	virgola posto	alla	fine	di	un	ciclo	 while 	sulla	stessa	riga.
Se	non	rendete	visibile	tale punto	e	virgola	indentandolo	su	una	sua	riga,	risulterà	invisibile.
{{% /note %}}

---

## Team Rules

{{% note %}}
Ogni	programmatore	ha	le	sue	regole	di	formattazione	preferite,	ma	se
lavora	in	un	team,	allora	prevale	il	team.
Un	team	di	sviluppatori	potrebbe	accordarsi	su	un	determinato	stile	di
formattazione	e	ogni	membro	di	tale	team	dovrà	adottare	tale	stile.
Vogliamo	che	il	software	abbia	uno	stile	coerente.
Ricordate:	un	buon	sistema	software	è	costituito	da	un	insieme	di
documenti	che	si	leggono	senza	problemi.	Devono	avere	uno	stile	coerente
e	coeso.	Chi	legge	deve	essere	sicuro	che	la	formattazione	che	troverà	in	un
file	di	codice	sorgente	la	ritroverà	negli	altri.	L’ultima	cosa	che	vogliamo
fare	è	complicare	il	codice	sorgente	scrivendolo	ognuno	con	il	proprio	stile.
{{% /note %}}
