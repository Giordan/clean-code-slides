+++
weight = 20
+++

{{% section %}}


# Clean Code

{{% note %}}

Ci sono due ragioni per cui stai leggendo questo libro, la prima perche' sei un programmatore
e la seconda e' perche' vuoi essere un programmatore migliore. Bene. Abbiamo bisogno di
programmatori migliori.

{{% /note %}}

---

## The Total Cost of Owning a Mess

{{% note %}}

Iniziamo parlando in realta' del codice cattivo, viene indicato con il principale problema dell'abbassamento della produttivita' di un team.<br />
Quando il codice e' degradato la produttivita' del team diminuisce ed il management fa l'unica cosa che puo' fare, aggiungere persone al team
sperando che questo aumenti la produttivita'. Ma questo non fa altro che ridurre la produttivita' perche' le nuove persone non conoscono bene il prodotto.<br />
Una frase che mi ha colpito e' "passare il tempo a mantenere pulito il tuo codice non e' solo conveniente, e' una quesione di sopravvivenza professionale."

{{% /note %}}

---

## What is Clean Code

{{% note %}}

La cattiva notizia e' che scrivere codice pulito e' come dipingere un quadro. Molti di noi sanno riconoscere la buona arte, ma questo non significa che sappiamo dipingere.<br />
Allo stesso modo essere in grado di riconoscere il codice pulito non implica che sappiamo scrivere codice pulito.<br />
Scrivere codice pulito richiede l'uso disciplinato di una miriade di piccole tecniche applicate attraverso un senso di "pulizia" accuratamente acquisito.
La chiave e' il "senso-del-codice", alcuni di noi ci nascono altri devono lottare per ottenerlo.<br />
Questo "senso" non solo ci permette di vedere se il codice e' buono o cattivo, ma ci mostra anche la strategia per applicare le trasformazioni che ci portano da codice cattivo a codice pulito.

{{% /note %}}

---

## What is Clean Code, some definitions by deeply experienced programmers

---

**Bjarne Stroustrup, inventor of C++ and author of The C++ Programming Language**

{{% fragment %}}Enfatizza come il codice debba essere elegante ed efficiente.{{% /fragment %}}

{{% fragment %}}"Clean code does one thing well"{{% /fragment %}}

---

**Grady Booch, author of Object Oriented Analysis and Design with Applications**

{{% fragment %}}Il codice pulito deve essere semplice e diretto. Non deve affuscare gli intenti del progettista. {{% /fragment %}}

{{% fragment %}}"Clean code reads like well-written prose"{{% /fragment %}}

---

**“Big” Dave Thomas, founder of OTI, godfather of the Eclipse strategy**


{{% fragment %}}Il codice pulito deve essere leggibile e migliorabile da uno sviluppatore che non sia l'autore originale.{{% /fragment %}}

{{% fragment %}}Dovrebbe avere test unitari e di accettazione altrimenti non puo' definirsi Clean Code.{{% /fragment %}}

---

**Michael Feathers, author of Working Effectively with Legacy Code**

{{% fragment %}}Il clean code sembra sempre scritto da qualcuno a cui importa, che ha cura del codice che scrive.{{% /fragment %}}

---

**Ron Jeffries, author of XP Installed**


{{% fragment class="small"%}}In ordine di priorita', il codice deve:{{% /fragment %}}

{{% fragment %}} 1 - far girare tutti i test{{% /fragment %}}

{{% fragment %}} 2 - non contenere duplicazione{{% /fragment %}}

{{% fragment %}} 3 - esprimere tutte le idee di design presenti nel sistema{{% /fragment %}}

{{% fragment %}} 4 - minimizzare il numero di entita' quali classi, metodi, funzioni ecc{{% /fragment %}}

{{% fragment %}}"Reduced duplication, high expressiveness, and early building of simple abstractions. That’s what makes clean code for me."{{% /fragment %}}


---

**Ward Cunningham, inventor of Wiki, inventor of Fit, coinventor of eXtreme Programming.**

{{% fragment %}}Sai che stai lavorando su codice pulito quando ogni routine che leggi risulta essere piu' o meno come ti aspetti.{{% /fragment %}}

{{% fragment %}}Si puo' chiamare codice "bello" quando tale codice fa sembrare che il linguaggio sia stato creato per risolvere quel problema. {{% /fragment %}}

---

## We Are Authors

{{% fragment %}}Gli autori sono responsabili di dover comunicare bene ai propri lettori.<br />La prossima volta che scrivi una riga di codice, ricorda che sei un autore, e che scrivi per dei lettori che giudicheranno il tuo lavoro.{{% /fragment %}}

{{% note %}}
Il rapporto tra il tempo che spendi a leggere il codice ed il tempo che spendi a scriverlo e' circa di 10:1. Noi leggiamo costantemente vecchio codice per poterne scrivere di nuovo.
Tu non puoi scrivere del nuovo codice se non leggi il codice che c'e' attorno.
{{% /note %}}

---

## The Boy Scout Rule

**"Leave the campground cleaner than you found it."**

{{% note %}}
La regola d'oro degli scout si puo' applicare anche al codice, lascia la codebase piu' pulita di come l'hai trovata. Non c'e' bisogno di cambiare grandi cose, basta anche un semplice nome di variabile, eliminare un po' di duplicazione. Questa e' una regola che innesca un circolo virtuoso.
{{% /note %}}



{{% /section %}}
