+++
weight = 50
+++

{{% section %}}

# Comments

{{% fragment %}}"Don't comment bad code, rewrite it"{{% /fragment %}}

---

## Comments Do Not Make Up for Bad Code

{{% note %}}
Una delle motivazioni che spingono a scrivere dei commenti e' la complessita' del codice a cui si riferiscono.
Ma in realta' e' meglio contare su un codice chiaro ed espressivo piuttosto che un codice complesso ed intricato e pieno di commenti.
Invece di spendere il tempo a commentare del codice intricato, districatelo piuttosto.
{{% /note %}}

---

## Explain Yourself in Code

{{% fragment %}}
```java
// Check to see if the employee is eligible for full benefits
if ((employee.flags & HOURLY_FLAG) &&
 (employee.age > 65))
```
{{% /fragment %}}

{{% fragment %}}
```java
if (employee.isEligibleForFullBenefits())
```
{{% /fragment %}}

{{% note %}}
Vi sono delle volte in cui il codice non riesce a spiegare se stesso. Spesso questa affermazione e' usata come alibi.
In molti casi si tratta semplicemente di creare una funzione che dice la stessa cosa del commento che si vuole inserire.
{{% /note %}}

---

## Good Comments

{{% fragment %}}
Keep in mind, however, that the only truly good comment is the comment you found a way not to write.
{{% /fragment %}}

---

**Legal Comments**

{{% fragment %}}
```php
/**
 * This file is part of the Caendra Payment Bundle.
 *
 * Created on 25/05/2018
 *
 * @copyright eLearnSecurity s.r.l.
 */
namespace Caendra\Bundle\PaymentBundle\Zuora\Manager;

use eLearnSecurity\Zuora\Object\Payment;
/**
 * class ZuoraPaymentManager.
 *
 * @author    Gabriele Giuranno aka giordan <gabriele.g@elearnsecurity.com>
 */
```
{{% /fragment %}}

{{% note %}}
Talvolta gli standard aziendali ci impongono di inserire dei commenti per motivi legali.
{{% /note %}}

---

**Clarification**

```java
public void testCompareTo() throws Exception
{
  WikiPagePath a = PathParser.parse("PageA");
  WikiPagePath ab = PathParser.parse("PageA.PageB");
  WikiPagePath b = PathParser.parse("PageB");
  WikiPagePath aa = PathParser.parse("PageA.PageA");
  WikiPagePath bb = PathParser.parse("PageB.PageB");
  WikiPagePath ba = PathParser.parse("PageB.PageA");
  assertTrue(a.compareTo(a) == 0); // a == a
  assertTrue(a.compareTo(b) != 0); // a != b
  assertTrue(ab.compareTo(ab) == 0); // ab == ab
  assertTrue(a.compareTo(b) == -1); // a < b
  assertTrue(aa.compareTo(ab) == -1); // aa < ab
  assertTrue(ba.compareTo(bb) == -1); // ba < bb
  assertTrue(b.compareTo(a) == 1); // b > a
  assertTrue(ab.compareTo(aa) == 1); // ab > aa
  assertTrue(bb.compareTo(ba) == 1); // bb > ba
}
```

{{% note %}}
Talvolta sembra opportuno tradurre il significato di uno strano argomento o di un valore di ritorno in qualcosa di leggibile.
Pero' si corre il rischio che il commento sia impreciso. Pertanto prima di scrivere commenti come questo, verificate se non ci sia un modo migliore
di procedere e in caso negativo verificate che i commenti siano precisi.
{{% /note %}}

---

**TODO Comments**


{{% note %}}
I "TODO" sono delle operazioni che il programmatore pensa andrebbero fatte, ma per qualche motivo lascia in sospeso.
Potrebbe trattarsi di una nota per cancellare una funzionalita' indesiderata o altri motivi.
Qualsiasi sia il significato del "todo". non e' una scusa per lasciare il cattivo codice all'interno del sistema.
Evitate che il vostro codice sia pieno di TODO. Controllateli regolarmente ed eliminate quelli che potete.
{{% /note %}}

---

## Bad Comments


{{% note %}}
La maggior parte dei commenti rientra in questa categoria, secondo l'autore. Solitamente sono solo dei pretesti per programmare malamente e giustificazioni di decisioni scadenti.
{{% /note %}}

---

**Mumbling**

{{% note %}}
Scrivere	un	commento	solo	perché	sentite	di	doverlo	fare	o	perché	il processo	lo	richiede,	è	del	tutto	inutile, dedicategli il	tempo	necessario	per	assicurarvi	che	sia	il	miglior commento	che	possiate	scrivere.
Ogni	commento	che	vi	costringe	a	cercare	in	un	altro	modulo	il significato	di	quel	commento	fallisce	nel	suo	tentativo	di	comunicare	con voi	e	non	vale	nemmeno	i	bit	che	consuma.
{{% /note %}}

---

**Misleading Comments**

```java
// Utility method that returns when this.closed is true. Throws an exception
// if the timeout is reached.
public synchronized void waitForClose(final long timeoutMillis)
throws Exception
{
  if(!closed)
  {
    wait(timeoutMillis);
    if(!closed)
    throw new Exception("MockResponseSender could not be closed");
  }
}
```

{{% note %}}
In	che	senso	il	commento	è	fuorviante?	Il	metodo	non	esce	 quando
this.closed 	diviene	 true .	Esce	 se this.closed 	è	 true ;	in	caso	contrario,
attende	un	certo	time-out	e	poi	lancia	un’eccezione	se	 this.closed 	non	è
ancora	 true .
Questa	subdola	imprecisione,	inserita	in	un	commento	che	è	più	difficile
da	leggere	del	corpo	del	codice,	può	far	sì	che	un	altro	programmatore
richiami	inavvertitamente	questa	funzione	aspettandosi	che	termini	non
appena	 this.closed 	diviene	 true .	Il	povero	programmatore	dovrà	così
svolgere	una	sessione	di	debug	nel	tentativo	di	scoprire	perché	il	suo	codice
è	così	lento.
{{% /note %}}

---

## Journal Comments

```java
* Changes (from 11-Oct-2001)
*
* 11-Oct-2001 : Re-organised the class and moved it to new package
* com.jrefinery.date (DG);
* 05-Nov-2001 : Added a getDescription() method, and eliminated NotableDate
* class (DG);
* 12-Nov-2001 : IBD requires setDescription() method, now that NotableDate
* class is gone (DG); Changed getPreviousDayOfWeek(),
* getFollowingDayOfWeek() and getNearestDayOfWeek() to correct
* bugs (DG);
* 05-Dec-2001 : Fixed bug in SpreadsheetDate class (DG);
* 29-May-2002 : Moved the month constants into a separate interface
* (MonthConstants) (DG);
* 27-Aug-2002 : Fixed bug in addMonths() method, thanks to N??levka Petr (DG)
```

{{% note %}}
Talvolta i	programmatori	aggiungono	un	commento	all' inizio	di un modulo	ogni	volta	che	lo	modificano.	Questi	commenti	si	accumulano come	in	un	log,	e	registrano ogni	modifica	svolta.
Con i sistemi di controllo del versionamento del codice, al giorno d'oggi, questi log non fanno altro che offuscare il modulo e quindi andrebbero completamente rimossi.
{{% /note %}}

---

**Noise Comments**

```java
/**
 * Default constructor.
 */
protected AnnualDateRule() {
}
```
```java
/** The day of the month. */
 private int dayOfMonth;
```
```java
/**
 * Returns the day of the month.
 *
 * @return the day of the month.
 */
public int getDayOfMonth() {
 return dayOfMonth;
}
```

{{% note %}}
Talvolta	si	trovano	commenti	che	non	servono	proprio	a	nulla.
Ribadiscono	l’ovvio	e	non	forniscono	alcuna	informazione.
Questi	commenti	sono	talmente	inutili	che	presto	impariamo	a	ignorarli.
Mentre	leggiamo	il	codice,	i	nostri	occhi	semplicemente	li	saltano.	E	poi	i
commenti	iniziano	a	mentire	quando	il	codice	attorno	a	loro	cambia.
{{% /note %}}

---

**Scary Noise**

```java
/** The name. */
private String name;
/** The version. */
private String version;
/** The licenceName. */
private String licenceName;
/** The version. */
private String info;
```

{{% note %}}
Leggete	questi	commenti	con	attenzione.	Lo	vedete	l’errore	copia-
incolla?	Se	gli	autori	non	dedicano	la	necessaria	attenzione	ai	commenti
mentre	li	scrivono	(o	incollano),	perché	mai	coloro	che	leggono	il	codice
dovrebbero	considerarli?
{{% /note %}}

---

**Don’t Use a Comment When You Can Use a Function or a Variable**

```java
// does the module from the global list <mod> depend on the
// subsystem we are part of?
if (smodule.getDependSubsystems().contains(subSysMod.getSubSystem()))
```

```java
ArrayList moduleDependees = smodule.getDependSubsystems();
String ourSubSystem = subSysMod.getSubSystem();
if (moduleDependees.contains(ourSubSystem))
```

---

**Position Markers** and **Closing Brace Comments**


{{% note %}}
Talvolta	i	programmatori	amano	contrassegnare	una	determinata posizione	in	un	file	di	codice	sorgente.
Talvolta	i	programmatori	inseriscono	dei	commenti	alle	parentesi	graffe
chiuse.	Questo	può	avere	senso	nelle	funzioni	molto
lunghe	con	strutture	profondamente	annidate,	ma	non	servono	a	nulla	nelle
piccole	funzioni	incapsulate	che	prediligiamo.	Pertanto,	se	pensate	di	dover
commentare	le	parentesi	graffe	chiuse,	cercate	piuttosto	di	accorciare	le
vostre	funzioni.
{{% /note %}}

---

**Attributions and Bylines**

```java
/* Added by Rick */
```
{{% fragment %}}
Il	sistema	di	controllo	del	codice	sorgente	è	il luogo	migliore per	questo	genere	di	informazioni.
{{% /fragment %}}

---

**Commented-Out Code**

{{% fragment %}}
Cancellate	il	codice.	Non	lo	perderete.	Fidatevi.<br />C'e' sempre il vostro amico <strike>mercurial</strike> git.
{{% /fragment %}}

{{% note %}}
Poche	abitudini	sono	altrettanto	fastidiose	del	porre	il	codice	in
commenti.	Non	fatelo!
Coloro	che	vedranno	quel	codice	commentato	non	avranno	il	coraggio	di
cancellarlo.	Penseranno	che	sia	lì	per	un	motivo	e	che	sia	troppo	importante
per	cancellarlo.	Pertanto	il	codice	commentato	finisce	per	accumularsi
come	il	fondo	in	una	pessima	bottiglia	di	vino.
Cancellate	il	codice.	Non	lo	perderete.	Fidatevi.
{{% /note %}}

---

**HTML Comments**

{{% note %}}
La	presenza	di	codice	HTML	nei	commenti	al	codice	sorgente	è	un abominio.
Complica la	leggibilità	dei	commenti	nel	posto	in	cui	dovrebbero	essere	più	facili	da leggere:	l’editor/IDE.
{{% /note %}}

---

**Nonlocal Information**

```java
/**
 * Port on which fitnesse would run. Defaults to <b>8082</b>.
 *
 * @param fitnessePort
 */
public void setFitnessePort(int fitnessePort)
{
this.fitnessePort = fitnessePort;
}
```

{{% note %}}
Se	dovete	scrivere	un	commento,	allora	assicuratevi	che	descriva	il
codice	circostante.	Non	offrite	informazioni	di	sistema	nel	contesto	di	un
commento	locale.
{{% /note %}}

---

**Too Much Information**

```java
/*
 RFC 2045 - Multipurpose Internet Mail Extensions (MIME)
 Part One: Format of Internet Message Bodies
 section 6.8. Base64 Content-Transfer-Encoding
 The encoding process represents 24-bit groups of input bits as output
 strings of 4 encoded characters. Proceeding from left to right, a
 24-bit input group is formed by concatenating 3 8-bit input groups.
 These 24 bits are then treated as 4 concatenated 6-bit groups, each
 of which is translated into a single digit in the base64 alphabet.
 When encoding a bit stream via the base64 encoding, the bit stream
 must be presumed to be ordered with the most-significant-bit first.
 That is, the first bit in the stream will be the high-order bit in
 the first 8-bit byte, and the eighth bit will be the low-order bit in
 the first 8-bit byte, and so on.
 */
 ```

{{% note %}}
Non	infilate	nei	commenti	i	dettagli	di	interessanti	discussioni	storiche	o
di	descrizioni	irrilevanti.
A	parte	il	numero	di	RFC,	chi	leggerà	questo codice	non	ha	alcun	bisogno	delle	esotiche	informazioni	contenute	nel commento.
{{% /note %}}


---

**Inobvious Connection**

```java
/*
* start with an array that is big enough to hold all the pixels
* (plus filter bytes), and an extra 200 bytes for header info
*/
this.pngBytes = new byte[((this.width + 1) * this.height * 3) + 200];
```

{{% note %}}
La	connessione	fra	il	commento	e	il	codice	che	descrive	dovrebbe	essere evidente.
Se	vi	date	il	disturbo	di	scrivere	un	commento,	quanto	vorreste che	chi	legge	sia	in	grado	di	vedere	il	commento	e	il	codice	e	di	capire	il
motivo	del	commento.
Lo	scopo	di	un	commento	è quello	di	spiegare	il	codice	che	non	si	spiega	da	solo.	È	un	peccato	quando un	commento	richiede…	spiegazioni.
{{% /note %}}

---

**Function Headers**

{{% note %}}
Le	funzioni	brevi	non	hanno	bisogno	di	troppe	descrizioni.	Un	nome
“parlante”	per	una	piccola	funzione	che	faccia	una	cosa	sola	è	solitamente
meglio	di	un	commento	di	intestazione.
{{% /note %}}

{{% /section %}}
