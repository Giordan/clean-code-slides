+++
weight = 10
+++

{{% section %}}

# Introduction

{{% note %}}
<p>Ci sono due parti nell' apprendimento del artigianato del software: la conoscenza ed il lavoro.</p>
<p>Devi conoscere i principi, i modelli, le pratiche che l'artigiano conosce e devi guadagnare la conoscenza sul campo
che si ottiene solo con il duro lavoro e la pratica.<br /><b>Esempio della bicicletta.</b></p>
<p>Ricordati che imparare a scrivere codice pulito e' un lavoro duro.</p>

{{% /note %}}

---

## Disclaimer
{{% note %}}
Questo libro mostra quello che gli autori hanno imparato in anni di lavoro nello sviluppo di software.<br />
Cioe' come loro praticano la loro "arte" e sostengono che se segui gli insegnamenti contenuti nel libro, godrai dei benefici
che loro hanno goduto e imparerai a scrivere codice pulito.
Potra capitare che sarai in disaccordo con alcuni concetti espressi.E questo va bene.

Inoltre, io vi presento quello che a me e' rimasto piu' impresso, cercando comunque di essere il piu' esaustivo possibile.
Al solito, con alcuni concetti si potra' essere in completo disaccordo, l'importante e' confrontarsi per ampliare le proprie "vedute".
{{% /note %}}

---

## The book structure

{{% note %}}
Questo libro si divide in 3 parti.<br />
La prima che comprende i primi capitoli descrive i principi, i modelli e le pratiche per la scrittura di codice pulito<br />
La seconda mostra dei casi d'uso dove vengono applicati i concetti spiegati nella prima parte<br />
Infine la terza mostra le casistiche e gli errori individuati durante la stesura dei casi d'uso.
{{% /note %}}

{{% /section %}}
