+++
weight = 40
+++

{{% section %}}

# Functions

{{% fragment %}}Functions are the first line of organization in any program.{{% /fragment %}}

---

## Small!

{{% fragment %}}The first rule of functions is that they should be small.<br />
The second rule of functions is that they should be smaller than that.{{% /fragment %}}

{{% note %}}
L'autore dice che questa affermazione non ha bisogno di giustificazioni, e che in 40 anni ha scritto funzioni di tutte le lunghezze.<br />
Racconta che negli anni 80 erano soliti dire che una funzione non doveva essere piu' grossa di un intera schermata che al tempo era composta da 24 linee(di cui 4 usate dall'editor) e 80 colonne.
{{% /note %}}

---

**Blocks and Indenting**

{{% note %}}
I blocchi all'interno degli if, else e while dovrebbero essere di una sola linea. E probabilmente questa linea sara' una chiamata a funzione.
Questo non solo tiene la funzione chiamante piu' piccola, ma funge anche da documentazione, perche' la funzione chiamata puo' avere una nome esplicativo.
Cio' implica anche che le funzioni non dovrebbero essere abbastanza grandi da contenere strutture nidificate. Pertanto il livello di "indentazione" di una funzione dovrebbe essere al massimo uno o due.
Questo, naturalmente, rende le funzioni piu' leggibili.
{{% /note %}}

---

## Do One Thing!

**FUNCTIONS SHOULD DO ONE THING. THEY SHOULD DO IT WELL.
THEY SHOULD DO IT ONLY.**

{{% note %}}
Le funzioni devono fare una cosa, farla bene e fare solo quella.
Il problema con quest'affermazione e' definire per bene cosa sia "una cosa".
Un modo per sapere se una funzione sta facendo piu' di una cosa e' se riesci ad estrarre una funzione con un nome che non e' semplicemente una riformulazione dell'implementazione che si sta estraendo.
Dopotutto, il motivo per cui scriviamo funzioni e' quello di scomporre un concetto piu' grande in una serie di passaggi al livello successivo di astrazione.
{{% /note%}}

---

**Sections within Functions**

{{% note %}}
Quando una funzione prevede delle sezioni quali dichiarazioni, inizializzazioni, ecc. mostra un chiaro sintomo del fatto che stia facendo piu' di una cosa.
Una funzione che fa una cosa sola non puo' essere ragionevolmente suddivisa in sezioni.
{{% /note %}}

---

## One Level of Abstraction per Function

{{% note %}}
Per essere sicuri che le nostre funzioni facciano una cosa sola, dobbiamo assicurarci che tutte le righe che compongono la funzione siano allo stesso livello di astrazione.
Mescolare i livelli di astrazione all'interno di una funzione e' fonte di confusione. Chi legge potrebbe non essere in grado di dire se un'espressione e' un concetto essenziale o un dettaglio.
Ancora peggio, come le finestre rotte di un palazzo, una volta che concetti a diverso livello di astrazione sono mescolati si tendera' ad avere sempre una maggiore mescolanza tra concetti di livello diverso.
{{% /note %}}

---

<strong>Reading Code from Top to Bottom:<br />The Stepdown Rule</strong>

{{% note %}}
L'intenzione e' quella di avere del codice leggibile come una narrazione dall'alto al basso. Cioe' si vuole che ogni funzione si seguita da quelle piu' in basso come livello di astrazione in modo tale da poter leggere il programma discendendo da un livello di astrazione al successivo. Questa viene chiamata la regola Stepdown.
Per dirla in modo differente, si vuole che il programma sia leggibile come un insieme di paragrafi TO, ognuno dei quali descrive il livello corrente di astrazione e faccia riferimento ai paragrafi TO appartenenti al livello di astrazione successivo.
Per gli sviluppatori e' molto difficile imparare a seguire questa regola e riuscire a scrivere funzioni che siano su un solo livello i astrazione. Ma secondo l'autore, imparare questa metodologia e' davvero importante per tenere le funzioni corte e che facciano una sola cosa.
{{% /note %}}

---

## Switch Statements

{{% note %}}
E' difficile mantenere piccolo lo switch e soprattutto e' difficile far fare ad uno switch una cosa sola. Per sua natura lo switch fa' piu' cose. Sfortunatamente non sempre si puo' fare a meno di usare switch, ma si puo' fare in modo da relegarlo in classi di basso livello ed evitare che sia ripetuto. Questo si puo' fare con il polimorfismo.
La regola generale dell'autore riguardo allo switch e' che viene tollerato se compare una volta sola e se viene usato per creare oggetti polimorfici e viene nascosto dietro a relazioni di ereditarieta' in modo che il resto del sistema non si accorga della sua presenza.L'autore stesso ammette pero' d aver violato qualche volta questa regola o parte di essa.
{{% /note %}}

---

## Use Descriptive Names

{{% note %}}
L'autore ci ricorda il principio espresso da Ward Cunningham, "sai di stare lavorando su codice pulito quando ogni routine risulta essere piu' o meno quello che ti aspetteresti."
Meta' del lavoro per raggiungere questo obiettivo e' scegliere nomi descrittivi per funzioni piccole che fanno una cosa sola.
Piu' e' piccola e focalizzata una funzione piu' e' facile scegliere un nome descrittivo.
Non si deve avere paura di usare nomi lunghi. Un nome lungo ma descrittivo e' meglio di un nome corto e criptico.
Inoltre, un nome lungo e descrittivo e' meglio di un commento lungo e descrittivo...
Non avere paura ad impiegare del tempo per scegliere i nomi. La scelta dei nomi chiarira' il design nella tua mente e ti aiutera' a migliorarlo.
Non e' affatto raro che la ricerca di un nome migliore poi si traduca in una ristrutturazione del codice.
Sii coerente con i nomi che usi nel tuo codice.
Una fraseologia coerente permette alla strutturazione delle tue funzioni di raccontare una "storia".
{{% /note %}}

---

## Function Arguments

{{% fragment %}}The ideal number of arguments for a function is zero (niladic).{{% /fragment%}}
{{% fragment %}}Next comes one (monadic), {{% /fragment%}}
{{% fragment %}}followed closely by two (dyadic).{{% /fragment%}}
{{% fragment %}}Three arguments (triadic) should be avoided where possible.{{% /fragment%}}
{{% fragment %}}More than three (polyadic) requires very special justification...{{% /fragment%}}

{{% note %}}
I parametri delle funzioni sono elementi delicati e difficili da gestire. Sono difficili da un punto di vista della testabilita'. Immaginate quanto possa essere difficile scrivere dei test che coprano le varie combinazioni di parametri di una funzione che ne prevede molti. Se non ci sono parametri il test sara' semplice, ma gia' con piu' di due parametri le combinazioni possibili aumentano considerevolmente.

Quando i paramtri sono usati come output la situazione si complica ulteriormente.(lo vedremo piu' avanti)
Siamo abituati quando leggiamo una funzione ad aspettarci che l'informazione entri attraverso i parametri ed esca attraverso il valore di ritorno, non tramite i parametri.
{{% /note%}}

---

**Common Monadic Form**

{{% fragment %}}
```java
boolean fileExists ("MyFile")
```
{{% /fragment %}}

{{% fragment %}}
```java
InputStream fileOpen("MyFile")
```
{{% /fragment %}}

{{% fragment %}}
```java
void passwordAttemptFailedNtimes(int attempts)
```
{{% /fragment %}}

{{% fragment %}}
```java
StringBuffer transform(StringBuffer in)
```
better than
```java
void transform-(StringBuffer out)
```
{{% /fragment %}}

{{% note %}}
Ci sono due ragioni molto comuni per usare delle chiamate a funzione con un solo parametro.
Potresti voler fare una domanda per il parametro
Oppure vorresti trasformare il parametro in qualche altra cosa e ritornare quest'ultima.
Una forma meno comune e' la gestione di un evento. Questa prevede un parametro in ingresso e nessun valore di ritorno ed e' molto utile quando si vuole modificare lo stato del sistema a seguito dell'evento.

Invece usare un parametro di output invece di un valore di ritorno in una funzione che effettua una trasformazione e' una cattiva idea. Infatti l'autore suggerice che... e' meglio di ...(ultimo frammento di questa slide)
{{% /note%}}

---

**Flag Arguments**

{{% note %}}
I parametri booleani sono davvero brutti e passare un booleano ad una funzione e' una pratica terribile, a detta dell'autore.
Complica automaticamente la signature del metodo e dichiara ad alta voce che quella funzione fa piu' di una cosa.
{{% /note %}}

---

**Dyadic Functions**

{{% fragment %}}A function with two arguments is harder to understand than a monadic function.{{% /fragment %}}

{{% note %}}
Le funzioni a due parametri non sono malvagie e sicuramente ci si trova spesso a scriverle, ma si deve essere coscienti che richiedono un costo e che andrebbero sfruttati i meccanismi che permettono di convertirle in funzioni con un solo parametro.
{{% /note %}}

---

**Triads**

{{% fragment %}}Functions that take three arguments are significantly harder to understand than dyads.{{% /fragment %}}

{{% note %}}
Le funzioni che prendono tre parametri sono molto più difficili da comprendere rispetto a quelle che ne prendono due.
I problemi sull'ordine dei parametri e il tempo per comprenderli ed eventualmente ignorarli e' ancora maggiore rispetto alle funzioni che accettano 2 parametri.
L'autore suggerisce di pensare molto attentamente prima di creare una funzione che accetta 3 parametri.
{{% /note %}}

---

**Argument Objects**

{{% fragment %}}
```java
Circle makeCircle(double x, double y, double radius);

Circle makeCircle(Point center, double radius);
```
{{% /fragment %}}

{{% note %}}
Quando una funzione sembra avere bisogno di due o tre parametri probabilmente alcuni di questi parametri andrebbero inseriti in una classe a parte.
Ridurre il numero di parametri creando oggetti che li raggruppano potrebbe sembrare come barare, ma non lo e'.
Quando un gruppo di parametri viene passato insieme, probabilmente fanno parte di uno stesso concetto che ha bisogno di un nome proprio.
{{% /note %}}

---

**Argument Lists**

{{% fragment %}}
```java
String.format("%s worked %.2f hours.", name, hours);
```
{{% /fragment %}}

{{% note %}}
A volte vogliamo passare un numero variabile di argomenti in una funzione. Se gli argomenti variabili sono tutti trattati in modo identico, come nell'esempio sopra, allora sono equivalenti a un singolo argomento di tipo List. Infatti la funzione String.format e' una funzione che accetta due parametri.
{{% /note %}}

---

**Verbs and Keywords**

{{% fragment %}}
``` java
write(name)
```
{{% /fragment %}}

{{% fragment %}}
```java
writeField(name)
```
{{% /fragment %}}

{{% note %}}
Scegliere un buon nome per una funzione puo' essere molto utile per spiegare le intenzioni della funzione, l'ordine e l'intento dei parametri.

Nel caso di una funzione con un solo parametro la funzione ed il parametro dovrebbero formare una coppia verbo/sostantivo.
Nell'ultimo esempio si vede l'uso di una keyword all'interno del nome della funzione, questo aiuta a specificare meglio il tipo del parametro.
{{% /note %}}

---

## Have No Side Effects

```java
public class UserValidator {
  private Cryptographer cryptographer;
  public boolean checkPassword(String userName, String password) {
    User user = UserGateway.findByName(userName);
    if (user != User.NULL) {
      String codedPhrase = user.getPhraseEncodedByPassword();
      String phrase = cryptographer.decrypt(codedPhrase, password);
      if ("Valid Password".equals(phrase)) {
        Session.initialize();
        return true;
      }
    }
    return false;
  }
}
```

{{% note %}}
Gli effetti collaterali sono bugie.La tua funzione promette di fare una cosa, ma fa anche altre cose nascoste.
La funzione checkPassword, con il suo nome, dice che controlla la password.
Il nome non implica che inizializzi la sessione.
Questo effetto collaterale crea un accoppiamento temporale.
Gli accoppiamenti temporali sono confusi, specialmente se nascosti come effetti collaterali.
Se devi avere un accoppiamento temporale, dovresti chiarire il nome della funzione.
In questo caso potremmo rinominare la funzione checkPasswordAndInitializeSession, anche se questo sicuramente viola il principio di "fare una cosa sola".
{{% /note %}}

---

**Output Arguments**

{{% fragment %}}
```java
appendFooter (s);
```
{{% /fragment %}}

{{% fragment %}}
```java
report.appendFooter();
```
{{% /fragment%}}

{{% note %}}
I parametri di solito sono interpretati come input per una funzione.
Ma vediamo la funzione appendFooter (s); Questa funzione aggiunge il pie' di pagina a qualcosa o aggiunge il pie' di pagina a s? Ed s cos'e'? un parametro di ingresso o uscita?andando a vedere la signature della funzione si capisce, ma comunque si perde del tempo.
Prima della programmazione OO era necessario avere dei parametri che fossero di output, ma adesso non e' piu' necessario. In altre parole si dovrebbe avere report.appendFooter();
In generale i parametri usati come output andrebbero evitati. Se la funzione deve cambiare lo stato di qualcosa che cambi lo stato dell'oggetto a cui appartiene.
{{% /note %}}

---

## Command Query Separation

{{% fragment %}}
```java
public boolean set(String attribute, String value);

if (set("username", "unclebob"))...
```
{{% /fragment %}}

{{% fragment %}}
```java
if (attributeExists("username")) {
      setAttribute("username", "unclebob");
      ...
}
```
{{% /fragment%}}

{{% note %}}
Le funzioni dovrebbero o fare qualcosa o rispondere a qualcosa, ma non entrambi.
O la tua funzione dovrebbe cambiare lo stato di un oggetto, o dovrebbe restituire alcune informazioni su quell'oggetto. Fare entrambi porta spesso alla confusione.
La funzione set potrebbe essere ambigua nell'ambito dell'if. La vera soluzione e' separare esplicitamente la parte di query dalla parte che modifica lo stato.
{{% /note %}}

---

## Prefer Exceptions to Returning Error Codes /1

```java
if (deletePage(page) == E_OK) {
  if (registry.deleteReference(page.name) == E_OK) {
    if (configKeys.deleteKey(page.name.makeKey()) == E_OK){
      logger.log("page deleted");
    } else {
      logger.log("configKey not deleted");
    }
  } else {
    logger.log("deleteReference from registry failed");
  }
} else {
  logger.log("delete failed");
  return E_ERROR;
}
```
{{% note %}}
Ritornare dei codici di errore dalle funzioni che modificano lo stato del sistema (funzioni di command) e' una sottile violazione del principio di "Command Query Separation" perche' promuove l'uso di queste funzioni nei costrutti if.

Questo esempio non soffre del problema di interpretare il nome (vedi set slide precedente) ma favorisce strutture fortemente annidate, perche' restituire un errore costringe il chiamante ad affrontarlo.

{{% /note %}}

---

## Prefer Exceptions to Returning Error Codes /2

```java
try {
  deletePage(page);
  registry.deleteReference(page.name);
  configKeys.deleteKey(page.name.makeKey());
} catch (Exception e) {
  logger.log(e.getMessage());
}
```
{{% note %}}
D'altro canto, se si utilizzano eccezioni invece di restituire codici di errore, il codice che andra' a gestirle potra' essere separato dal codice che gestisce il caso corretto e potra' essere semplificato.
{{% /note %}}

---

**Extract Try/Catch Blocks**

```java
public void delete(Page page) {
  try {
    deletePageAndAllReferences(page);
  } catch (Exception e) {
    logError(e);
  }
}
private void deletePageAndAllReferences(Page page) throws Exception {
  deletePage(page);
  registry.deleteReference(page.name);
  configKeys.deleteKey(page.name.makeKey());
}
private void logError(Exception e) {
  logger.log(e.getMessage());
}
```

{{% note %}}
I blocchi try/catch mescolano la gestione degli errori con la logica applicativa.E' meglio estrarre questi blocchi in funzioni separate.
{{% /note %}}

---

**Error Handling Is One Thing**

```java
public void delete(Page page) {
  try {
    deletePageAndAllReferences(page);
  } catch (Exception e) {
    logError(e);
  }
}
...
```

{{% note %}}
Le funzioni dovrebbero fare una cosa sola e la gestione degli errori sono una cosa, quindi una funzione che gestisce gli errori dovrebbe fare solo quello e nient'altro.
Cio' implica, come nell'esempio che se la keyword try esiste dentro una funzione, sara' la prima e non ci dovrebbe essere nulla dopo i  blocchi catch/finally
{{% /note %}}

---

## Don’t Repeat Yourself

{{% note %}}
La duplicazione puo' essere la radice di tutto il male nel software. Infatti, sono stati creati molti principi e pratiche con lo scopo di controllarla ed eliminarla.
Pensa alle tutte le forme normali dei database, servono per eliminare la duplicazione del dato oppure la programmazione object oriented che serve per concentrare il codice in classi di base che altrimenti sarebbero ridondanti.

Sembrerebbe che dall'invenzione della subroutine, le innovazioni nello sviluppo del sofware siano state un tentativo continuo di eliminare la duplicazione dal nostro codice sorgente.

{{% /note %}}

---

## How Do You Write Functions Like This?

{{% note %}}
Scrivere software e' come ogni altro tipo di scrittura. Inizialmente butti giu' le idee e poi le "massaggi" finche' non sono facilmente leggibili. La prima bozza potrebbe risultare maldestra e disorganizzata, ma andando avanti con la scrittura, la riscrittura il testo si raffina fino a raggiungere un buon livello di legibilita'.

L'autore dice che quando inizia a scrivere le funzioni gli riescono lunghe e complicate, con un sacco di cicli annidati e lunghe liste di parametri. Con nomi arbitrari ed un sacco di duplicazione. Ma l'autore dice anche di avere un sacco di test unitari che "coprono" quel codice.
Quindi, massaggia e raffina il codice, dividendo funzioni, cambiando nomi ed eliminando duplicazione.
Alla fine si ritrova con le funzioni che seguono le regole descritte prima.
{{% /note %}}

---

## Conclusion

{{% fragment %}}The art of programming is, and has always been, the art of language design.{{% /fragment %}}
{{% note %}}
Ogni sistema e' costruito da uno specifico linguaggio di dominio progettato dai programmatori per descrivere quel sistema. Le funzioni sono i verbi di questa lingua e le classi sono i nomi.
L'arte della programmazione e', ed e' sempre stata, l'arte del design del linguaggio.
I programmatori esperti, dice l'autore, pensano ai sistemi come alle storie da raccontare piuttosto che ai programmi da scrivere.
Usano le strutture messe a disposizione dal linguaggio di programmazione per crearne uno piu' ricco ed espressivo. Parte del linguaggio di dominio e' la gerarchia delle funzioni che descrivono le azioni compiute all'interno del sistema.
In questo capitolo si e' visto la meccanica della scrittura. Se si seguono le regole qui descritte si avranno sicuramente delle funzioni brevi, ben definite e ben organizzate.

{{% /note %}}

{{% /section %}}
